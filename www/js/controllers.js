angular.module('starter.controllers', [])

.controller('mainCtrl', function($scope) {
  $scope.openCam = function() {
    window.location = "cam.html";
  };

  $scope.openQr = function() {
    window.location = "scan.html";
  };
})

.controller('qrCtrl', function($scope, $cordovaBarcodeScanner) {
  $scope.scanQr = function() {
      $cordovaBarcodeScanner.scan().then(function(data) {
          $scope.dummy = data.text
          $scope.qrObj = JSON.parse($scope.dummy);

          console.log("Barcode Format: " + data.format);
          console.log("Cancelled: " + data.cancelled);
          console.log("text:" + data.text);
          console.log("json:"+ $scope.qrObj.E);
          console.log("jsonL:" + $scope.qrObj.L)
      }, function(error) {
          console.log("An error happened: " + error);
      });
  };

  $scope.backToMain = function() {
    window.location = "index.html";
  };
})

.controller('imageController', function($scope, $cordovaCamera, $cordovaFile, $ionicPopup) {

    $scope.images = [];

    $scope.deletePopup = function(index) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Delete Image',
        template: 'Are you sure you want to delete this Image?'
      });

      confirmPopup.then(function(res) {
        if(res) {
          $scope.removeImage(index);
        } else {
          confirmPopup.close();
        }
      });
    }

    $scope.removeImage = function(index) {
      $scope.images.splice(index, 1);
    };

    $scope.addImage = function() {
      var options = {
        destinationType : Camera.DestinationType.FILE_URI,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : false,
        encodingType: Camera.EncodingType.JPEG,

      };

      $cordovaCamera.getPicture(options).then(function(imageData) {

        onImageSuccess(imageData);

        function onImageSuccess(fileURI) {
          createFileEntry(fileURI);
        }

        function createFileEntry(fileURI) {
          window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
        }



        function copyFile(fileEntry) {
          var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
          var newName = makeid() + name;

          window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(fileSystem2) {
            fileEntry.copyTo(
              fileSystem2,
              newName,
              onCopySuccess,
              fail
            );
          },
          fail);
        }

        function onCopySuccess(entry) {
          $scope.$apply(function () {
            $scope.images.push(entry.nativeURL);
          });
        }

        function fail(error) {
          console.log("fail: " + error.code);
        }

        function makeid() {
          var text = "";
          var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

          for (var i=0; i < 5; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
          }
          return text;
        }

      }, function(err) {
        console.log(err);
      });
    }

    $scope.urlForImage = function(imageName) {
      var name = imageName.substr(imageName.lastIndexOf('/') + 1);
      var trueOrigin = cordova.file.dataDirectory + name;
      return trueOrigin;
    }

    $scope.backToMain = function() {
      window.location = "index.html";
    }
});
